# Doc

Daily usage annotations

## Manuals

### Kubernetes

- [Jenkins](kubernetes/jenkins/README.md)
- [SonarQube](kubernetes/sonarqube/README.md)
- [Kaniko](kubernetes/kaniko/README.md)
- [ngrok](kubernetes/ngrok/README.md)

## Lists

- [WordList](wordlist/eff_large_wordlist.txt)
