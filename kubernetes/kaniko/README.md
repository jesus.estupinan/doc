# Kaniko

Build Container Images In Kubernetes

## Usage

Write a secret in kubernetes with docker credentials

```console
kubectl create secret generic kaniko-secret --from-file $HOME/.docker/config.json
```

Edit your `job` file in kubernetes, example [kaniko-job.example.yaml](kaniko-job.example.yaml)

then:

```console
kubectl apply -f kaniko-job.yml
```

## Source code

[Available](https://github.com/GoogleContainerTools/kaniko) under license [Apache-2.0](https://raw.githubusercontent.com/GoogleContainerTools/kaniko/main/LICENSE)
