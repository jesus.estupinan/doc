# Ngrok

ngrok is the programmable network edge that adds connectivity,
security, and observability to your apps with no code changes.

> Remember that you must have an ngrok account to get a token

## Secrets

### How to find the secret name

You can get the secret name as follows:

```console
kubectl get secrets
```

This will produce an output similar to:

```bash
NAME                  TYPE                                  DATA      AGE
admin-user-pass       Opaque                                2         11s
```

### Adding a Secret in Kubernetes

```console
kubectl create secret generic ngrok-secret --from-file $HOME/.config/ngrok/ngrok.yml
```

### Deleting a Secret in Kubernetes

```console
kubectl delete secret ngrok-secret
```

## Usage

### Test app (nginx)

Start by creating the nginx Deployment

```console
kubectl apply -f kubernetes/ngrok/nginx-deployment.yaml
```

Expose it inside the cluster by creating a corresponding Service (typeClusterIP)

```console
kubectl apply -f kubernetes/ngrok/nginx-service.yaml
```

### Enter ngrok

To expose the nginx Service we just created, we can create a ngrok deployment
which will run the ngrok process with an HTTP tunnel to the nginx
Service (using the Service name)

Create the ngrok Deployment

```console
kubectl apply -f kubernetes/ngrok/ngrok-deployment.yaml
```

Now you need to extract the ngrok URL. The below command does a couple of things

* gets the Pod for the ngrok Deployment
* uses the HTTP endpoint inside of the ngrok Pod to get the
  details (using kubectl exec inside the running Pod)

```console
kubectl exec $(kubectl get pods -l=app=ngrok -o=jsonpath='{.items[0].metadata.name}') -- curl -s http://localhost:4040/api/tunnels
```

Output will be similar to what you see below — refer to the public_url
field to grab the ngrok URL accessible via public internet
(in this example, it's <https://b42658ec.ngrok.io>)

```json
{
  "tunnels": [
    {
      "name": "command_line",
      "uri": "/api/tunnels/command_line",
      "public_url": "https://b42658ec.ngrok.io",
      "proto": "https",
      "config": {
        "addr": "http://nginx-service:80",
        "inspect": true
      },
      "metrics": {
        "conns": {
          "count": 0,
          "gauge": 0,
          "rate1": 0,
          "rate5": 0,
          "rate15": 0,
          "p50": 0,
          "p90": 0,
          "p95": 0,
          "p99": 0
        },
        "http": {
          "count": 0,
          "rate1": 0,
          "rate5": 0,
          "rate15": 0,
          "p50": 0,
          "p90": 0,
          "p95": 0,
          "p99": 0
        }
      }
    },
    {
      "name": "command_line (http)",
      "uri": "/api/tunnels/command_line%20%28http%29",
      "public_url": "http://b42658ec.ngrok.io",
      "proto": "http",
      "config": {
        "addr": "http://nginx-service:80",
        "inspect": true
      },
      "metrics": {
        "conns": {
          "count": 0,
          "gauge": 0,
          "rate1": 0,
          "rate5": 0,
          "rate15": 0,
          "p50": 0,
          "p90": 0,
          "p95": 0,
          "p99": 0
        },
        "http": {
          "count": 0,
          "rate1": 0,
          "rate5": 0,
          "rate15": 0,
          "p50": 0,
          "p90": 0,
          "p95": 0,
          "p99": 0
        }
      }
    }
  ],
  "uri": "/api/tunnels"
}
```

## Contributions

Based in [article ngrok in kubernetes](https://archive.is/aPfo3) of website [medium.com](https://medium.com)
