# SonarQube

## Encode USERNAME and PASSWORD of Postgres using following commands

```console
echo -n "postgresadmin" | base64
```

```console
echo -n "admin123" | base64
```

## Create the Secret using kubectl apply

```console
kubectl apply -f postgres-secrets.yml
```

## Create PV and PVC for Postgres using yaml file

```console
kubectl apply -f postgres-storage.yaml
```

## Deploying Postgres with kubectl apply

```console
kubectl apply -f postgres-deployment.yaml
```

```console
kubectl apply -f postgres-service.yaml
```

## Create PV and PVC for SonarQube

```console
kubectl apply -f sonar-pv-data.yml
```

```console
kubectl apply -f sonar-pv-extensions.yml
```

```console
kubectl apply -f sonar-pvc-data.yml
```

```console
kubectl apply -f sonar-pvc-extentions.yml
```

## Create configmaps for URL which we use in SonarQube

```console
kubectl apply -f sonar-configmap.yaml
```

## Deploy SonarQube

```console
kubectl apply -f sonar-deployment.yml
```

```console
kubectl apply -f sonar-service.yml
```

## Check secrets

```console
kubectl get secrets
```

```console
kubectl get configmaps
```

```console
kubectl get pv
```

```console
kubectl get pvc
```

```console
kubectl get deploy
```

```console
kubectl get pods
```

```console
kubectl get svc
```

Now Go To LoadBalancer and check whether service comes Inservice or not,
If it comes Inservice copy DNS Name of LoadBalancer and check in web UI

## Default Credentials for SonarQube

```bash
UserName: admin
PassWord: admin
```

Now we can cleanup by using below commands:

```console
kubectl delete deploy postgres sonarqube
```

```console
kubectl delete svc postgres sonarqube
```

```console
kubectl delete pvc postgres-pv-claim sonar-data sonar-extensions
```

```console
kubectl delete pv postgres-pv-volume sonar-pv-data sonar-pv-extensions
```

```console
kubectl delete configmaps sonar-config
```

```console
kubectl delete secrets postgres-secrets
```
